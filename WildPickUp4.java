public class WildPickUp4 extends Wilds implements SkipEffect{
    public void useAbility(){
        skipEffect();
        addFour();
    }

    public void skipEffect(){
        System.out.println("Skips your turn");
    }

    public void addFour(){
        System.out.println("Adds Four");
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof WildPickUp4){
            return true;
        }else{
            return false;
        }
    } 

    @Override
    public String toString(){
        return "WildPickUp4"; 
    }
}
