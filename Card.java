public interface Card {
    boolean canPlay(Card card);
    String getColour();
    boolean equals(Object o);
    String toString();
}
