public abstract class Wilds implements Card{
    protected String newColour;

    public Wilds(){
        this.newColour = null;
    }

    @Override
    public boolean canPlay(Card card){
        assignColour();
        return true;
    }

    @Override
    public String getColour(){
        return this.newColour;
    }

    public void assignColour(){

    }

    @Override
    public abstract boolean equals(Object o);
    
    @Override
    public abstract String toString();
}
