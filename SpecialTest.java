import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class SpecialTest {
    
    @Test
     public void getColourTest(){
         Card card = new Reverse(Colours.BLUE.toString());
         assertEquals(card.getColour(), "BLUE");
     }

     @Test
     public void canPlaySameColour(){
         Card card = new Skip(Colours.YELLOW.toString());
         Card card2 = new Reverse(Colours.YELLOW.toString());
         assertTrue(((Skip)card).canPlay(card2));
     }

     @Test
     public void canPlaySameSpecialSkip(){
         Card card = new Skip(Colours.YELLOW.toString());
         Card card2 = new Skip(Colours.BLUE.toString());
         assertTrue(((Skip)card).canPlay(card2));
     }
     
     @Test
     public void canPlaySameSpecialPickUp2(){
         Card card = new PickUp2(Colours.YELLOW.toString());
         Card card2 = new PickUp2(Colours.BLUE.toString());
         assertTrue(((PickUp2)card).canPlay(card2));
     }
     
     @Test
     public void canPlaySameSpecialReverse(){
         Card card = new Reverse(Colours.YELLOW.toString());
         Card card2 = new Reverse(Colours.BLUE.toString());
         assertTrue(((Reverse)card).canPlay(card2));
     }

     @Test
     public void canPlaySameColourWithNumber(){
         Card card = new Reverse(Colours.YELLOW.toString());
         Card card2 = new NumberedCard(Colours.YELLOW.toString(),1);
         assertTrue(((Reverse)card).canPlay(card2));
     }

     @Test
     public void canPlaySameColourWithNumberFail(){
         Card card = new Reverse(Colours.GREEN.toString());
         Card card2 = new NumberedCard(Colours.YELLOW.toString(),1);
         assertFalse(((Reverse)card).canPlay(card2));
     }
}
