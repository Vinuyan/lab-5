public abstract class SpecialCard extends ColouredCard{
    public abstract void useAbility();

    public SpecialCard(String colour){
        super(colour);
    }

    @Override
    public abstract boolean equals(Object o);

    @Override
    public abstract String toString();
}
