import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class DeckTest {  
     public static void main(String[] args) {
        Deck deck = new Deck();
        System.out.println(deck);
        deck.shuffle();
        System.out.println(deck);
        System.out.println(deck.getSize());
     }

     @Test
     public void drawTest(){
        Deck deck = new Deck();
        Card orignialCard = deck.getCard(0);
        Card drawnCard = deck.draw();
        assertEquals(drawnCard, orignialCard);
     } 

     @Test
     public void drawHalfwayTest(){
        Deck deck = new Deck();
        Card orignialCard = deck.getCard(10);
        Card drawnCard = null;
        for (int i = 0; i < 11; i++) {
            drawnCard = deck.draw();
        }
        assertEquals(drawnCard, orignialCard);
     } 

     @Test
     public void drawFinalTest(){
        Deck deck = new Deck();
        int size = deck.getSize();
        Card orignialCard = deck.getCard(size-1);
        Card drawnCard = null;

        for (int i = 0; i < size; i++) {
            drawnCard = deck.draw();
        }
        assertEquals(drawnCard, orignialCard);
     } 

     @Test
     public void addCardTest(){
        Deck deck = new Deck();
        Card newCard = new WildCard();
        deck.addToDeck(newCard);
        assertEquals(newCard, deck.getCard(deck.getSize()-1));;
     } 
}
