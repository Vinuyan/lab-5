import java.util.ArrayList;
import java.util.Random;

public class Deck {
    private ArrayList<Card> deckCards = new ArrayList<Card>();

    public Deck(){
        String[] colours = new String[]{Colours.BLUE.toString(),Colours.RED.toString(),Colours.GREEN.toString(),Colours.YELLOW.toString()};
        for (String string : colours) {
            for (int i = 0; i < 10; i++) {
                this.deckCards.add(new NumberedCard(string, i));
                this.deckCards.add(new NumberedCard(string, i));
            }
            for (int i = 0; i < 2; i++) {
                this.deckCards.add(new Reverse(string));
                this.deckCards.add(new PickUp2(string));
                this.deckCards.add(new Skip(string));
            }
        }

        for (int i = 0; i < 4; i++) {
            this.deckCards.add(new WildCard());
            this.deckCards.add(new WildPickUp4());
        }
        
    }


    public void addToDeck(Card card){
        this.deckCards.add(card);
    }

    public Card draw(){
        Card card = this.deckCards.get(0);
        this.deckCards.remove(0);
        return card;
    }

    public void shuffle(){
        Random rand = new Random();
        Card temp;
        int initPosition;
        int finalPosition;

        for (Card card : this.deckCards) {
            initPosition = this.deckCards.indexOf(card);
            finalPosition = rand.nextInt(this.deckCards.size());
            temp = this.deckCards.get(finalPosition);
            this.deckCards.set(finalPosition, card);
            this.deckCards.set(initPosition, temp);
        }
    }

    //For Tests
    public Card getCard(int index){
        return this.deckCards.get(index);
    }

    public int getSize(){
        return this.deckCards.size();
    }

    @Override
    public String toString(){
        String output = "";
        for (Card card : this.deckCards) {
            output += card.toString() + "\n";
        }
        return output;
    }
}
