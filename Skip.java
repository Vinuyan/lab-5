public class Skip extends SpecialCard implements SkipEffect{
    public Skip(String colour){
        super(colour);
    }

    @Override
    public boolean canPlay(Card card){
        if(card instanceof Skip){
            return true;
        } 
        
        if(this.colour.equals(card.getColour())){
            return true;
        } else{
            return false;
        }
    }

    public void useAbility(){
        skipEffect();
    }

    //helper method to useAbility
    public void skipEffect(){
        System.out.println("Skips your turn");
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof Skip){
            if(((Skip)o).getColour().equals(this.colour)){
                return true;
            } else{
                return false;
            }
        }else{
            return false;
        }
    } 

    @Override
    public String toString(){
        return "Skip, Colour: "+this.colour; 
    }
}
