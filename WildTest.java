import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class WildTest {
    @Test
     public void getColourWildPickUp4(){
         Card card = new WildPickUp4();
         assertEquals(((Wilds)card).getColour(), null);
     }

     @Test
     public void getColourWildCard(){
         Card card = new WildCard();
         assertEquals(((Wilds)card).getColour(), null);
     }

    @Test
     public void canPlayOnNumberWild(){
         Card card = new WildCard();
         Card card2 = new NumberedCard(Colours.YELLOW.toString(),1);
         assertTrue(((WildCard)card).canPlay(card2));
     }

     @Test
     public void canPlayOnNumber4PickUp(){
         Card card = new WildPickUp4();
         Card card2 = new NumberedCard(Colours.YELLOW.toString(),1);
         assertTrue(((WildPickUp4)card).canPlay(card2));
     }
}
