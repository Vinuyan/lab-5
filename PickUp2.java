public class PickUp2  extends SpecialCard implements SkipEffect{
    public PickUp2(String colour){
        super(colour);
    }

    @Override
    public boolean canPlay(Card card){
        if(card instanceof PickUp2){
            return true;
        } 
        
        if(this.colour.equals(card.getColour())){
            return true;
        } else{
            return false;
        }
    }

    public void useAbility(){
        skipEffect();
        plusTwo();
    }

    //helper method to useAbility
    public void skipEffect(){
        System.out.println("Skips your turn");
    }

    //helper method to useAbility
    public void plusTwo(){
        System.out.println("Pick up 2");
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof PickUp2){
            if(((PickUp2)o).getColour().equals(this.colour)){
                return true;
            } else{
                return false;
            }
        }else{
            return false;
        }
    }  

    @Override
    public String toString(){
        return "PickUp2, Colour: "+this.colour; 
    }

}
