import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class NumberedCardTest {
     @Test
     public void getColourTest(){
         Card card = new NumberedCard(Colours.BLUE.toString(), 1);
         assertEquals(card.getColour(), "BLUE");
     }

     @Test
     public void getNumberTest(){
         Card card = new NumberedCard(Colours.BLUE.toString(), 1);
         assertEquals(((NumberedCard)card).getNumber(), 1);
     }

     @Test
     public void canPlaySameNumber(){
         Card card = new NumberedCard(Colours.BLUE.toString(), 1);
         Card card2 = new NumberedCard(Colours.YELLOW.toString(), 1);
         assertTrue(((NumberedCard)card).canPlay(card2));
     }

     @Test
     public void canPlaySameColour(){
         Card card = new NumberedCard(Colours.YELLOW.toString(), 8);
         Card card2 = new NumberedCard(Colours.YELLOW.toString(), 1);
         assertTrue(((NumberedCard)card).canPlay(card2));
     }

     @Test
     public void canPlaySameNumberFail(){
         Card card = new NumberedCard(Colours.BLUE.toString(), 8);
         Card card2 = new NumberedCard(Colours.YELLOW.toString(), 1);
         assertFalse(((NumberedCard)card).canPlay(card2));
     }

     @Test
     public void canPlaySameColourFail(){
         Card card = new NumberedCard(Colours.BLUE.toString(), 8);
         Card card2 = new NumberedCard(Colours.YELLOW.toString(), 1);
         assertFalse(((NumberedCard)card).canPlay(card2));
     }

     @Test
     public void canPlaySameColourSpecial(){
         Card card = new NumberedCard(Colours.YELLOW.toString(), 8);
         Card card2 = new Reverse(Colours.YELLOW.toString());
         assertTrue(((NumberedCard)card).canPlay(card2));
     }
}
