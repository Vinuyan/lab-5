public abstract class ColouredCard implements Card {
    protected String colour;

    public ColouredCard(String colour){
        this.colour = colour;
    }

    @Override
    public String getColour(){
        return this.colour;
    }

    @Override
    public abstract boolean canPlay(Card card);

    @Override
    public abstract boolean equals(Object o);

    @Override
    public abstract String toString();
}
