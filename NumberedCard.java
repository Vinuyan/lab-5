public class NumberedCard extends ColouredCard{
    private int number;

    public NumberedCard(String colour, int number){
        super(colour);
        this.number = number;
    }

    public int getNumber(){
        return this.number;
    }

    @Override
    public boolean canPlay(Card card){
        if(card instanceof NumberedCard){
            if(this.number == ((NumberedCard)card).getNumber()){
                return true;
            }
        } 
        
        if(this.colour.equals(card.getColour())){
            return true;
        } else{
            return false;
        }
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof NumberedCard){
            if(((NumberedCard)o).getNumber() == this.number && ((NumberedCard)o).getColour().equals(this.colour)){
                return true;
            } else{
                return false;
            }
        }else{
            return false;
        }
    }  

    @Override
    public String toString(){
        return "NumberedCard, Colour: "+this.colour+ ", Number:" + this.number; 
    }
}
