public class Reverse extends SpecialCard{

    public Reverse(String colour){
        super(colour);
    }

    @Override
    public boolean canPlay(Card card){
        if(card instanceof Reverse){
            return true;
        } 
        
        if(this.colour.equals(card.getColour())){
            return true;
        } else{
            return false;
        }
    }

    public void useAbility(){
        reverseEffect();
    }

    //helper method to useAbility
    public void reverseEffect(){
        System.out.println("Reversed turn order");
    }

    @Override
    public boolean equals(Object o){
        if(o instanceof Reverse){
            if(((Reverse)o).getColour().equals(this.colour)){
                return true;
            } else{
                return false;
            }
        }else{
            return false;
        }
    } 

    @Override
    public String toString(){
        return "Reverse, Colour: "+this.colour; 
    }
}
